package com.soprasteria.progettodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.PropertySource;



@SpringBootApplication
@EnableCaching
@EnableAutoConfiguration
@PropertySource(ignoreResourceNotFound = true, value =     "classpath:/src/main/resurces/application.properties")
public class ControlloApplication {

	public static void main(String[] args) {
		SpringApplication.run(ControlloApplication.class, args);//commento. nuovo conflitto
	}
}
