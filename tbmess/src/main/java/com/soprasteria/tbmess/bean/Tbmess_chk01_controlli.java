package com.soprasteria.tbmess.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Tbmess_chk01_controlli {
	
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Long Id;

	private Date datarif;
	private String centroapplmitt;
	private String tipofile;
	private String tipocontrollo;
	private String filemancante;
	private Timestamp timestampcreazfile;
	private String esitocontrollo;
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public Date getDatarif() {
		return datarif;
	}
	public void setDatarif(Date datarif) {
		this.datarif = datarif;
	}
	public String getCentroapplmitt() {
		return centroapplmitt;
	}
	public void setCentroapplmitt(String centroapplmitt) {
		this.centroapplmitt = centroapplmitt;
	}
	public String getTipofile() {
		return tipofile;
	}
	public void setTipofile(String tipofile) {
		this.tipofile = tipofile;
	}
	public String getTipocontrollo() {
		return tipocontrollo;
	}
	public void setTipocontrollo(String tipocontrollo) {
		this.tipocontrollo = tipocontrollo;
	}
	public String getFilemancante() {
		return filemancante;
	}
	public void setFilemancante(String filemancante) {
		this.filemancante = filemancante;
	}
	public Timestamp getTimestampcreazfile() {
		return timestampcreazfile;
	}
	public void setTimestampcreazfile(Timestamp timestampcreazfile) {
		this.timestampcreazfile = timestampcreazfile;
	}
	public String getEsitocontrollo() {
		return esitocontrollo;
	}
	public void setEsitocontrollo(String esitocontrollo) {
		this.esitocontrollo = esitocontrollo;
	}
	public String getFlagemailsend() {
		return flagemailsend;
	}
	public void setFlagemailsend(String flagemailsend) {
		this.flagemailsend = flagemailsend;
	}
	public Timestamp getTmstemailsend() {
		return tmstemailsend;
	}
	public void setTmstemailsend(Timestamp tmstemailsend) {
		this.tmstemailsend = tmstemailsend;
	}
	public Timestamp getTmstinsert() {
		return tmstinsert;
	}
	public void setTmstinsert(Timestamp tmstinsert) {
		this.tmstinsert = tmstinsert;
	}
	private String flagemailsend;
	private Timestamp tmstemailsend;
	private Timestamp tmstinsert;
	
	
	
	
	
	
	
	
}
