package com.soprasteria.tbmess.service;
import java.util.List;

import com.soprasteria.tbmess.bean.Tbmess_chk01_controlli;
public interface ControlloService {
	
	List<Tbmess_chk01_controlli> listAll();//read
	Tbmess_chk01_controlli getById(Long Id);//read
	Tbmess_chk01_controlli saveOrUpdate(Tbmess_chk01_controlli controllo);//create / update
	void delete(String key);//delete
	
	
	

}
