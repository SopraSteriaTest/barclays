package com.soprasteria.tbmess.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.soprasteria.tbmess.bean.Tbmess_chk01_controlli;
import com.soprasteria.tbmess.repository.ControlloRepository;
import java.util.ArrayList;
import java.util.List;



@Service
public class ControlloServiceImpl implements ControlloService{

	private ControlloRepository c;
	
	@Autowired
	public ControlloServiceImpl(ControlloRepository c)
	{
		this.c=c;
	}
	@Autowired
	 @Override
		public List<Tbmess_chk01_controlli> listAll()
		  {
		      List<Tbmess_chk01_controlli> Controllo=new ArrayList();
		      c.findAll().forEach(Controllo::add);
		      return Controllo;
		  }
	

	

	public Tbmess_chk01_controlli saveOrUpdate(Tbmess_chk01_controlli controllo) {
		
		return null;
	}

	public void delete(String key) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public Tbmess_chk01_controlli getById(Long Id) {
		Tbmess_chk01_controlli t=new Tbmess_chk01_controlli();
		t=c.findOne(Id);
		return t;
	}
	
}
