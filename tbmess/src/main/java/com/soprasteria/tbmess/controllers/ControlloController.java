package com.soprasteria.tbmess.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.config.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.soprasteria.tbmess.bean.ApiResponse;
import com.soprasteria.tbmess.bean.Tbmess_chk01_controlli;
import com.soprasteria.tbmess.repository.ControlloRepository;
@RestController
@CrossOrigin(origins = "http://127.0.0.1")
@RequestMapping("/api/v1")
public class ControlloController {
	@Autowired
	private ControlloRepository con;
	private static final Logger LOGGER = LogManager.getLogger(ControlloController.class);
	
	  @GetMapping("/Controllo")
	  public ApiResponse<Tbmess_chk01_controlli> getAllControlli() throws NullPointerException{
		  return new ApiResponse<Tbmess_chk01_controlli>(HttpStatus.OK.value(), "Richiesta accettata su protocollo HTTPS",con.findAll());
	  }
	 
	  
	  @GetMapping("/Controllo/{id}")
	  public ResponseEntity<Tbmess_chk01_controlli> getControlloById(@PathVariable(value = "id") Long Tbmess_chk01_controlliId)
	      throws ResourceNotFoundException {
		  Tbmess_chk01_controlli controllo =
	       con
	            .findOne(Tbmess_chk01_controlliId);
	    return ResponseEntity.ok().body(controllo);
	   
	  }
	  
	  @PostMapping("/New")
	  public Tbmess_chk01_controlli createControllo( @RequestBody Tbmess_chk01_controlli controllo) {
		  LOGGER.info(controllo.toString());
	    return con.save(controllo);
	  }
	  
	  @GetMapping("/GetPage/{ElementsForPage}/{numberofpage}")
	  public ApiResponse<Tbmess_chk01_controlli> getPage(@PathVariable(value = "ElementsForPage") Long Elements,@PathVariable(value = "numberofpage") Long Tbmess_chk01_controllipage)
	  {
		  List<Tbmess_chk01_controlli> pagina=new ArrayList();
		  LOGGER.info("ArrayList creato");
			for(Long i=(long) Tbmess_chk01_controllipage*(Long)Elements;i<(Tbmess_chk01_controllipage*(Long)Elements)+(Long)Elements;i++)
			{
				LOGGER.info("Query eseguita");
				pagina.add(con.findOne(i+1));
			}
			 return new ApiResponse<Tbmess_chk01_controlli>(HttpStatus.OK.value(), "Richiesta accettata su protocollo HTTPS", pagina);
	  }

}
