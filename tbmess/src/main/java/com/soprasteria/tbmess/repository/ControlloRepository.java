package com.soprasteria.tbmess.repository;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.soprasteria.tbmess.bean.Tbmess_chk01_controlli;

public interface ControlloRepository extends CrudRepository<Tbmess_chk01_controlli, Long> {
	
}
