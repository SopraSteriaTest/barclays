package com.test.repoTest;




import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.soprasteria.tbmess.bean.Tbmess_chk01_controlli;
import com.soprasteria.tbmess.repository.ControlloRepository;

import java.math.BigDecimal;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class ControlloRepositoryTest {

    private static final BigDecimal BIG_DECIMAL_100 = BigDecimal.valueOf(100.00);
    private static final String PRODUCT_DESCRIPTION = "a cool product";
    private static final String IMAGE_URL = "http://an-imageurl.com/image1.jpg";

    @Autowired
    private ControlloRepository controlloRepository;

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void testPersistence() {
        //given
    	Tbmess_chk01_controlli controllo = new Tbmess_chk01_controlli();
    	controllo.setId((long) 1);
    	controllo.setTipofile("test");
        //when
        controlloRepository.save(controllo);

        //then
        Assert.assertNotNull(controllo.getId());
        Tbmess_chk01_controlli newControllo = controlloRepository.findOne(controllo.getId());
        Assert.assertEquals((Long) 1L, newControllo.getId());
        Assert.assertEquals("test", newControllo.getTipofile());
       
    }
}
